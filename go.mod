module gitlab.com/sascha.l.teichmann/xlsx-test

go 1.16

require (
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.4.0 // indirect
	github.com/PaesslerAG/gval v1.1.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.7 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
