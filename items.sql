BEGIN;

CREATE TABLE owners (
    lastname varchar PRIMARY KEY,
    firstname varchar NOT NULL
);

CREATE TABLE items (
    owner varchar NOT NULL REFERENCES owner(lastname),
    item varchar NOT NULL,
    quantity integer NOT NULL
);

INSERT INTO owners (lastname, firstname) VALUES
  ('Pan', 'Peter'),
  ('Duck', 'Dagobert');

INSERT INTO items (owner, item, quantity) VALUES
  ('Pan', 'Anti aging cream', 10),
  ('Duck', 'Gold', 1000000),
  ('Duck', 'Number One Dime', 1);

END;
