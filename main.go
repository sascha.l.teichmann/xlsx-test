package main

import (
	"bufio"
	"context"
	"database/sql"
	"flag"
	"log"
	"os"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
	"gopkg.in/yaml.v2"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	var (
		input  string
		output string
		prg    string
		dbname string
	)

	flag.StringVar(&input, "input", "", "input XLSX")
	flag.StringVar(&input, "i", "", "input XLSX (shorthand)")
	flag.StringVar(&output, "output", "", "ouput XLSX")
	flag.StringVar(&output, "o", "", "output XLSX (shorthand)")
	flag.StringVar(&prg, "program", "", "template program")
	flag.StringVar(&prg, "p", "", "template program (shorthand)")
	flag.StringVar(&dbname, "d", "data.sqlite", "database (shorthand)")
	flag.StringVar(&dbname, "database", "data.sqlite", "database")

	flag.Parse()

	var inp *excelize.File
	var err error

	if input != "" {
		inp, err = excelize.OpenFile(input)
		if err != nil {
			log.Fatalf("Cannot open file '%s': %v\n", input, err)
		}
	} else {
		inp, err = excelize.OpenReader(bufio.NewReader(os.Stdin))
		if err != nil {
			log.Fatalf("Cannot read file from STDIN: %v\n", err)
		}
	}

	var program Action

	if prg != "" {
		if err := func() error {
			f, err := os.Open(prg)
			if err != nil {
				return err
			}
			defer f.Close()
			return yaml.NewDecoder(bufio.NewReader(f)).Decode(&program)
		}(); err != nil {
			log.Fatalf("Cannot parse template program '%s': %v\n", prg, err)
		}
	}

	db, err := sql.Open("sqlite3", dbname)
	if err != nil {
		log.Fatalf("Cannot open database '%s': %v\n", dbname, err)
	}
	defer db.Close()

	if err := program.template(context.Background(), db, inp); err != nil {
		log.Fatalf("Cannot template: %v\n", err)
	}

	if output != "" {
		if err := inp.SaveAs(output); err != nil {
			log.Fatalf("ERROR: Cannot write '%s': %v\n", output, err)
		}
	} else {
		out := bufio.NewWriter(os.Stdout)
		if _, err := inp.WriteTo(out); err != nil {
			log.Fatalf("ERROR: Cannot write to STOUT: %v\n", err)
		}
		if err := out.Flush(); err != nil {
			log.Fatalf("ERROR: Cannot flush STOUT: %v\n", err)
		}
	}

}
