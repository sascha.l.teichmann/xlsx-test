package main

import "strings"

func handlebars(s string, replace func(string) string) string {

	var (
		out, repl strings.Builder
		mode      int
	)

	for _, c := range s {
		switch mode {
		case 0:
			if c == '{' {
				mode = 1
			} else {
				out.WriteRune(c)
			}
		case 1:
			if c == '{' {
				mode = 2
			} else {
				out.WriteByte('{')
				out.WriteRune(c)
				mode = 0
			}
		case 2:
			switch c {
			case '\\':
				mode = 3
			case '}':
				mode = 4
			default:
				repl.WriteRune(c)
			}
		case 3:
			repl.WriteRune(c)
			mode = 2
		case 4:
			if c == '}' {
				out.WriteString(replace(repl.String()))
				repl.Reset()
				mode = 0
			} else {
				repl.WriteByte('}')
				repl.WriteRune(c)
				mode = 2
			}
		}
	}

	switch mode {
	case 1:
		out.WriteByte('{')
	case 2:
		out.WriteString("{{")
		out.WriteString(repl.String())
	case 3:
		out.WriteString("{{")
		out.WriteString(repl.String())
		out.WriteByte('\\')
	case 4:
		out.WriteString("{{")
		out.WriteString(repl.String())
		out.WriteByte('}')
	}

	return out.String()
}
