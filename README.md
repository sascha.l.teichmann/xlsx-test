# Simple test with XLSX replacements

# Run

```sh
sqlite3 data.sqlite < items.sql
go build
./xlsx-test -d data.sqlite -i items.xlsx -o output.xlsx -p items.yaml
```
